### Laravel-RolesUser is a Complete Build of Laravel with FULL Email and Social Authentication - COMPLETE WORKING Implementation.

### Laravel-RolesUser Screenshots
Login
![Login.png](https://bitbucket.org/repo/yaBbxR/images/2649637162-Login.png)
### Register page ###
![Register.png](https://bitbucket.org/repo/yaBbxR/images/2422081290-Register.png)
### Activate Email Address ###
![ActivateEmailAddress.png](https://bitbucket.org/repo/yaBbxR/images/612813652-ActivateEmailAddress.png)
### Reset Password ###
![ResetPassword.png](https://bitbucket.org/repo/yaBbxR/images/1375759238-ResetPassword.png)
### Reset Your Password Email ###
![ResetYourPasswordEmail.png](https://bitbucket.org/repo/yaBbxR/images/2701657962-ResetYourPasswordEmail.png)
### Reset Password once clicked on link ###
![ResetPasswordWithLink.png](https://bitbucket.org/repo/yaBbxR/images/1242341246-ResetPasswordWithLink.png)
### Admin Show this User ###
![AdminShowUserPage.png](https://bitbucket.org/repo/yaBbxR/images/4125426323-AdminShowUserPage.png)
### Admin Create New User Dropdown ###
![ResetYourPasswordEmail.png](https://bitbucket.org/repo/yaBbxR/images/1910328116-ResetYourPasswordEmail.png)
### Admin Create New User Page ###
![AdminCreateNewUserPage.png](https://bitbucket.org/repo/yaBbxR/images/4270602395-AdminCreateNewUserPage.png)
### Admin Create New User Access Level ###
![AdminCreateNewUserPageUAL.png](https://bitbucket.org/repo/yaBbxR/images/2460189937-AdminCreateNewUserPageUAL.png)
### Admin User Created ###
![AdminUserCreated.png](https://bitbucket.org/repo/yaBbxR/images/1391156741-AdminUserCreated.png)
### Admin Edit Users View ###
![AdminEditUsersView.png](https://bitbucket.org/repo/yaBbxR/images/2096812215-AdminEditUsersView.png)
### Admin Show this User ###
![AdminShowSelectedUserView.png](https://bitbucket.org/repo/yaBbxR/images/462840829-AdminShowSelectedUserView.png)

# Detail info #

Laravel 5x with user authentication and authorisation,  registration with email confirmation, Facebook, Twitter, and Google authentication, password recovery, and google captcha protection. This additionally makes full utilization of Controllers for the routes, templates for the views, and makes utilization of middleware for directing. Super simple setup, should be possible in 20 minutes. Just fill the Facebook, Twitter, and Google API Keys info .env file then ready to go :)

###### Updates:
* Added eloquent editable user profile
* Added IP Capture
* Added Google Maps API v3 for User Location lookup
* Added CRUD(Create, Read, Update, Delete) User Management

###### A [Laravel](http://laravel.com/) 5.x with minimal [Bootstrap](http://getbootstrap.com) 3.5.x project.
| Laravel-RolesUser Features  |
| :------------ |
|Built on [Laravel](http://laravel.com/) 5.x|
|Uses [MySQL](https://github.com/mysql) Database|
|Uses [Artisan](http://laravel.com/docs/5.1/artisan) to manage database migration, schema creations, and create/publish page controller templates|
|Dependencies are managed with [COMPOSER](https://getcomposer.org/)|
|Laravel Scaffolding **User** and **Administrator Authentication**.|
|User Registration with email verification|
|User roles implementation|
|User Login with remember password|
|User Password Reset|
|User Socialite Logins ready to go - See API list below|
|Google Captcha Protection|
|Capture IP to users table upon signup|
|Eloquent user profiles|
|CRUD (Create, Read, Update, Delete) User Management|
|Custom 404 Page|


|Add User Location Geocoding and Map|

### Quick Project Setup
###### (Not including the dev environment)
1. Run `git clone https://bitbucket.com/kris1/laravel-rolesuser.git laravel-rolesuser`
2. Create a MySQL database for the project
    * ```mysql -u root -p```,
    * ```create database laravel-rolesuser;```
    * ```\q```
3. From the projects root run `cp .env.example .env`
4. Configure your `.env` file // NOTE: Google API Key will prevent maps error
5. Run `composer update` from the projects root folder
6. From the projects root folder run `chmod -R 755 ../laravel-rolesuser`
7. From the projects root folder run `php artisan key:generate`
8. From the projects root folder run `php artisan migrate`
9. From the projects root folder run `composer dump-autoload`
10. From the projects root folder run `php artisan db:seed`

###### Seeds
1. Seeded Roles
   * user
   * editor
   * administrator

And thats it with the create of setting up and configuring your development environemnt.

### Laravel-RolesUser URL's (routes)
* ```/```
* ```/auth/login```
* ```/auth/logout```
* ```/auth/register```
* ```/password/email```


### Laravel-RolesUser Alias Redirect URL's (routes)
* ```/home```
* ```/login```
* ```/logout```
* ```/register```
* ```/reset```

### Get Socialite Login API Keys:
* [Google Captcha API] (https://www.google.com/recaptcha/admin#list)
* [Facebook API] (https://developers.facebook.com/)
* [GitHub API] (https://github.com/settings/applications/new)
* [37 Signals API] (https://github.com/basecamp/basecamp-classic-api)
* [Twitter API] (https://apps.twitter.com/)
* [Google &plus; API] (https://console.developers.google.com/)
* [YouTube API] (https://developers.google.com/youtube/v3/getting-started)
* [Twitch TV API] (http://www.twitch.tv/kraken/oauth2/clients/new)
* [Instagram API] (https://instagram.com/developer/register/)


### Other API keys
* [Google Maps API v3 Key] (https://developers.google.com/maps/documentation/javascript/get-api-key#get-an-api-key)

### Add More Socialite Logins
* See full list of providers: [http://socialiteproviders.github.io](http://socialiteproviders.github.io/#providers)
###### **Steps**:
  1. Go to [http://socialiteproviders.github.io](http://socialiteproviders.github.io/providers/twitch/) and select the provider to be added.
  2. From the projects root folder in terminal run compser command to get the needed package.
     * Example:
      ```
         composer require socialiteproviders/twitch
      ```
  3. From the projects root folder run ```composer update```
  4. Add the service provider to ```/app/services.php```
     * Example:
     ```
   	'twitch' => [
   	    'client_id' 	=> env('TWITCH_KEY'),
   	    'client_secret' => env('TWITCH_SECRET'),
   	    'redirect' 		=> env('TWITCH_REDIRECT_URI'),
   	],
     ```
  5. Add the API credentials to ``` /.env  ```
     * Example:
      ```
         TWITCH_KEY=YOURKEYHERE
         TWITCH_SECRET=YOURSECRETHERE
         TWITCH_REDIRECT_URI=http://YOURWEBSITEURL.COM/social/handle/twitch
      ```
  6. Add the social media login link:
      * Example:
      In file ```/resources/views/auth/login.blade.php``` add ONE of the following:
         * Conventional HTML:
      ```

         <a href="{{ route('social.redirect', ['provider' => 'twitch']) }}" class="btn btn-lg btn-primary btn-block twitch">Twitch</a>

      ```
         * Use Laravel HTML Facade (recommended)
      ```

         {!! HTML::link(route('social.redirect', ['provider' => 'twitch']), 'Twitch', array('class' => 'btn btn-lg btn-primary btn-block twitch')) !!}

      ```

--

## Environment File

Example `.env` file:
```
APP_ENV=local
APP_DEBUG=true
APP_KEY=SomeRandomString

DB_HOST=localhost
DB_DATABASE=homestead
DB_USERNAME=YOURDATABSEusernameHERE
DB_PASSWORD=YOURDATABSEpasswordHERE

CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync

MAIL_DRIVER=smtp
MAIL_HOST=smtp.gmail.com
MAIL_PORT=465
MAIL_USERNAME=YOURGMAILusernameHERE
MAIL_PASSWORD=YOURGMAILpasswordHERE
MAIL_ENCRYPTION=tls

# https://www.google.com/recaptcha/admin#list
RE_CAP_SITE=YOURGOOGLECAPTCHAsitekeyHERE
RE_CAP_SECRET=YOURGOOGLECAPTCHAsecretHERE

# https://developers.facebook.com/
FB_ID=YOURFACEBOOKidHERE
FB_SECRET=YOURFACEBOOKsecretHERE
FB_REDIRECT=http://yourwebsiteURLhere.com/social/handle/facebook

# https://apps.twitter.com/
TW_ID=YOURTWITTERidHERE
TW_SECRET=YOURTWITTERkeyHERE
TW_REDIRECT=http://yourwebsiteURLhere.com/social/handle/twitter

# https://console.developers.google.com/
GOOGLE_ID=YOURGOOGLEPLUSidHERE
GOOGLE_SECRET=YOURGOOGLEPLUSsecretHERE
GOOGLE_REDIRECT=http://yourwebsiteURLhere.com/social/handle/google
```

### File Structure of Common Used Files
```
laravel-rolesuser/
    ├── .env.example
    ├── .gitattributes
    ├── .gitignore
    ├── artisan
    ├── composer.json
    ├── gulpfile.js
    ├── LICENSE
    ├── package.json
    ├── phpspec.yml
    ├── phpunit.xml
    ├── README.md
    ├── server.php
    ├── app/
    │   ├── Http/
    │   │  ├── kernal.php
    │   │  ├── routes.php
    │   │  ├── Controllers/
    │   │  │   ├── Auth/
    │   │  │   │   ├── AuthController.php
    │   │  │   │   └── PasswordController.php
    │   │  │   ├── Controller.php
    │   │  │   ├── HomeController.php
    │   │  │   ├── ProfilesController.php
    │   │  │   ├── UserController.php
    │   │  │   ├── UsersManagementController.php
    │   │  │   └── WelcomeController.php
    │   │  ├── Middleware/
    │   │  │   ├── Administrator.php
    │   │  │   ├── Authenticate.php
    │   │  │   ├── CheckCurrentUser.php
    │   │  │   ├── Editor.php
    │   │  │   ├── EncryptCookies.php
    │   │  │   ├── RedirectAuthenticated.php
    │   │  │   └── VerifyCsrfToken.php
    │   │  └── Requests/
    │   │      └── Request.php
    │   ├── Logic/
    │   │   ├── macros.php
    │   │   ├── Mailers/
    │   │   │   ├── Mailer.php
    │   │   │   └── UserMailer.php
    │   │   └── User/
    │   │       ├── CaptureIp.php
    │   │       └── UserRepository.php
    │   ├── Models/
    │   │   ├── Password.php
    │   │   ├── Profile.php
    │   │   ├── Role.php
    │   │   ├── Social.php
    │   │   └── User.php
    │   ├── Providers/
    │   │   ├── AppServiceProvider.php
    │   │   ├── BusServiceProvider.php
    │   │   ├── ConfigServiceProvider.php
    │   │   ├── EventServiceProvider.php
    │   │   ├── MacroServiceProvider.php
    │   │   └── RouteServiceProvider.php
    │   ├── Services/
    │   │   └── Registrar.php
    │   └── Traits/
    │       └── CaptchaTrait.php
    ├── config/
    │   ├── app.php
    │   ├── auth.php
    │   ├── cache.php
    │   ├── compile.php
    │   ├── database.php
    │   ├── filesystems.php
    │   ├── mail.php
    │   ├── queue.php
    │   ├── services.php
    │   ├── session.php
    │   └── view.php
    ├── database/
    │   ├── migrations/
    │   │   ├── 2016_06_02_000000_create_users_table.php
    │   │   ├── 2016_06_02_100000_create_password_resets_table.php
    │   │   ├── 2016_06_10_124334_update_users_table.php
    │   │   ├── 2016_06_15_173121_create_users_roles.php
    │   │   ├── 2016_06_15_173333_create_user_role.php
    │   │   ├── 2016_06_15_173520_create_social_logins.php
    │   │   ├── 2016_06_16_004932_create_profiles_table.php
    │   │   ├── 2016_06_16_010553_add_signup_ip_address_to_users_table.php
    │   │   ├── 2016_06_16_011117_add_signup_confirmation_ip_address_to_users_table.php
    │   │   ├── 2016_06_16_025231_add_signup_sm_ip_address_to_users_table.php
    │   │   └── 2016_06_19_045644_add_signup_admin_ip_address_to_users_table.php
    │   └── seeds/
    │       ├── DatabaseSeeder.php
    │       └── SeedRoles.php
    ├── public/
    │   ├── .htaccess
    │   ├── index.php
    │   ├── robots.txt
    │   └── assets/
    │       ├── css/
    │       ├── fonts/
    │       └── ~~js/~~
    ├── resources/
    │   ├── assets/
    │   │   └── Less/
    │   │       ├── bootstrap/
    │   │       └── app.less
    │   ├── lang/
    │   │   └── en/
    │   │       ├── auth.php
    │   │       ├── emails.php
    │   │       ├── forms.php
    │   │       ├── links-and-buttons.php
    │   │       ├── modals.php
    │   │       ├── pagination.php
    │   │       ├── passwords.php
    │   │       ├── profile.php
    │   │       ├── titles.php
    │   │       └── validation.php
    │   └── views/
    │       ├── app.blade.php
    │       ├── welcome.blade.php
    │       ├── admin/
    │       │   ├── create-user.blade.php
    │       │   ├── edit-user.blade.php
    │       │   ├── edit-users.blade.php
    │       │   ├── show-user.blade.php
    │       │   └── show-users.blade.php
    │       ├── auth/
    │       │   ├── activateAccount.blade.php
    │       │   ├── guest_activate.blade.php
    │       │   ├── login.blade.php
    │       │   ├── password.blade.php
    │       │   ├── register.blade.php
    │       │   ├── reset.blade.php
    │       │   └── tooManyEmails.blade.php
    │       ├── emails/
    │       │   ├── activateAccount.blade.php
    │       │   └── password.blade.php
    │       ├── errors/
    │       │   ├── 403.blade.php
    │       │   ├── 404.blade.php
    │       │   └── 503.blade.php
    │       ├── modals/
    │       │   ├── modal-delete.blade.php
    │       │   └── modal-save.blade.php
    │       ├── pages/
    │       │   ├── home.blade.php
    │       │   └── status.blade.php
    │       ├── partials
    │       │   ├── form-status.blade.php
    │       │   └── nav.blade.php
    │       ├── profiles
    │       │   ├── edit.blade.php
    │       │   └── show.blade.php
    │       └── scripts
    │           ├── delete-modal-script.blade.php
    │           ├── gmaps-address-lookup-api3.blade.php
    │           ├── google-maps-geocode-and-map.blade.php
    │           └── save-modal-script.blade.php
    ├── storage/
    ├── tests/
    └── vendor/
```

---

#### Laravel Developement Packages Used References
* [Documentation and Usage](https://github.com/illuminate/html)
* http://laravel.com/docs/5.1/authentication
* http://laravel.com/docs/5.1/authorization
* http://laravel.com/docs/5.1/routing
* http://laravel.com/docs/5.0/schema

---

## [Laravel](http://laravel.com/) PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.png)](https://travis-ci.org/laravel/framework) [![Latest Stable Version](https://poser.pugx.org/laravel/framework/version.png)](https://packagist.org/packages/laravel/framework) [![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.png)](https://packagist.org/packages/laravel/framework) [![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, and caching.

Laravel aims to make the development process a pleasing one for the developer without sacrificing application functionality. Happy developers make the best code. To this end, we've attempted to combine the very best of what we have seen in other web frameworks, including frameworks implemented in other languages, such as Ruby on Rails, ASP.NET MVC, and Sinatra.

Laravel is accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

### Official Laravel Documentation

Documentation for the entire framework can be found on the [Laravel website](http://laravel.com/docs).

### Contributing To Laravel

**All Laravel Framework related issues and pull requests should be filed on the [laravel/framework](http://github.com/laravel/framework) repository.**

### Laravel License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

## [Bootstrap](http://getbootstrap.com) Front-End Framework

[![Build Status](https://img.shields.io/travis/twbs/bootstrap/master.svg)](https://travis-ci.org/twbs/bootstrap) ![Bower version](https://img.shields.io/bower/v/bootstrap.svg) [![npm version](https://img.shields.io/npm/v/bootstrap.svg)](https://www.npmjs.com/package/bootstrap) [![devDependency Status](https://img.shields.io/david/dev/twbs/bootstrap.svg)](https://david-dm.org/twbs/bootstrap#info=devDependencies) [![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Bootstrap is a sleek, intuitive, and powerful front-end framework for faster and easier web development, created by [Mark Otto](https://twitter.com/mdo) and [Jacob Thornton](https://twitter.com/fat), and maintained by the [core team](https://github.com/orgs/twbs/people) with the massive support and involvement of the community.

[Bootstrap](http://getbootstrap.com)'s documentation, included in this repo in the root directory, is built with [Jekyll](http://jekyllrb.com) and publicly hosted on GitHub Pages at [<http://getbootstrap.com>](http://getbootstrap.com).

---

## Development Environement References and help

If you do not have Bower, it can be installed using Node Package Manager (NPM).
If you do not have NPM, it can be installed using NODE JS.

###Install NODE JS
####Node JS can be installed muliple ways:
GUI Installer, easiest way (Simply [Download](https://nodejs.org/en/) and Install)

####Node JS can also be installed using Homebrew Package Manager with the following command:
```
brew install node
```

###Install Node Package Manager (NPM)
####NPM can be installed using the following command:
```
npm install -g bower
```

###Install Bower
####Bower can be installed with the following command:
```
npm install -g bower
```

###Install GULP
####GULP can be installed using the following command:
```
npm install -g gulp
```
### Host uniqe domain locally 
###### Example  - The last line is the important part of the example
```
##
# Host File
#
# localhost is used to configure the loopback interface
# when the system is booting.  Do not change this entry.
##
127.0.0.1    localhost
127.0.0.1    kris1.co.nz
```

---

## Cheers

###### ~ **Kris**